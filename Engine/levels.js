'use strict';


// This module contains the sprites you'll create for your level tiles.  Sprites are given a unique id number - so in these examples cobblestone has id of 1, grass has an id of 2.
// *** ADD YOUR OWN TILE SPRITES HERE - GIVE EACH TILE SPRITE A UNIQUE ID ***
Game.Sprites = (function(sprites) {
  
  // Tiles
  sprites[1] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/ice/ice01.png' });
  
  // -------------------------
  
  sprites[2] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow01.png' }); //
  
  sprites[3] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_left01.png' });
  sprites[4] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_left02.png' });
  sprites[5] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_left03.png' });
  sprites[6] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_mid01.png' }); //
  sprites[7] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_right01.png' });
  sprites[8] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_right02.png' });
  sprites[9] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_right03.png' });
  
  sprites[10] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_left04.png' });
  sprites[11] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_mid04.png' }); //
  sprites[12] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_right04.png' });
  
  sprites[13] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_single01.png' });
  sprites[14] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_top_single02.png' });
  
  sprites[15] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/snow/snow_bottom_mid01.png' });
  
  // -------------------------
  
  sprites[16] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock01.png' }); //
  
  sprites[17] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_top_left01.png' });
  sprites[18] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_top_left02.png' });
  sprites[19] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_top_mid01.png' }); //
  sprites[20] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_top_right01.png' });
  sprites[21] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_top_right02.png' });
  
  sprites[22] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_top_single01.png' });
  
  sprites[23] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/platforms/rock/rock_bottom_mid01.png' });
  
  
  // -------------------------
 
  sprites[24] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/hidden_tile.png'} );
  
  // -------------------------
  
 
  return sprites;

})(Game.Sprites || []);


// The Levels module contains the data needed to create an actual level, including the backgrounds we want to use as well as the level tile layout, player, NPC and platform setup.
Game.Levels = (function(module) {
  
  module.Level = (module.Level || []);
  
  // This example sets up a single level (with unique id 1 for 'level 1' in this case!)
  module.Level[1] = {
    
    name : 'Spooky level 1',
    
    // background provides a viewport aligned image (This remains fixed as you move around the level)
    // *** REPLACE THIS WITH YOUR OWN BACKGROUND IMAGE HERE ***
    background : {
      
      image : Game.Graphics.Sprite( {imageURL:'Assets/Images/FullMoon.jpg'} ),
      maintainAspect : true
    },
    
    // We can add different overlays on top of the background image.
    overlays : {
      
      sigma : 2.0,
      
      // Render layers ordered front-to-back
      // *** ADD YOUR OWN OVERLAYS HERE ***
      layers : [
    
        {
          image : Game.Graphics.Sprite({ imageURL : 'Assets/Images/Tio_Aimar/bg_snow.png' }),
          mode : RenderLevelImageMode.Fill, // default
          sigmaOverride : 0.35
          //periodicity : 2 
        },
        /*
        {
          image : Game.Graphics.Sprite({ imageURL : 'Assets/Images/SpookyCreatures01.png' }),
          mode : RenderLevelImageMode.Normal,
          offset : { x : -400, y : -200 },
          sigmaOverride : 0.5
          //periodicity : 1
        }
        */
    
      ]
    },
    
    // The level is made up of tiles - this tells the game engine tiles are 32 x 32 pixels in size
    // *** WHEN YOU CREATE YOUR OWN TILE SPRITES MAKE SURE THEY'RE THE SAME SIZE!!! ***
    tileWidth : 32,
    tileHeight : 32,
    
    // The level itself is an array (or collection) or tiles.  Here we're saying we want a level that's 32 tiles across and 12 tiles vertically.
    mapDimension : { width : 40, height : 24 },
    
    // mapTileArray is the actual implementation of the level design.  Each number in the array represents a different object...
    // 0 = empty space
    // 1 to 32 represent tiles
    // >32 repesent static scene elements
    // -1 indicates where the player will start
    // <-1 represent the pickup types (see game_objects.js for the pickup types and their id values)
    // *** DESIGN YOUR LEVEL AROUND A TILE ARRAY.  EACH TILE WILL HAVE A UNIQUE ID AS ABOVE.  WHERE YOU WANT THE TILE TO APPEAR IN THE LEVEL ADD THE ID TO THE ARRAY ***
    mapTileArray : (function() {
    
      let mapImage = [
      
      /* 0*/ [ 6, -2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -4,  0,  0,  0,  0,  0,  1,  1,  1,  0,  1],
      /* 1*/ [ 2, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -3,  0,  0,  0,  1,  0,  0,  0,  0, -4,  1],
      /* 2*/ [ 2,  8,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -2,  0,  0,  0,  1,  0,  1,  0,  1,  1,  1],
      /* 3*/ [ 2, -4,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  1,  0,  0,  0,  1],
      /* 4*/ [ 2,  8,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  0,  1,  1,  0,  1,  1,  0,  1,  1,  0,  1,  0, 24,  0, 24,  0, 24,  0,  0,  0,  1, -2,  1, -3,  0,  0,  1],
      /* 5*/ [ 2, -2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -3, -4,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  0,  1],
      /* 6*/ [ 2,  8,  0,  0,  0,  0,  0,  0,  0, -6,  0, -2, -3, -4, -2,  0,  0, -2, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -2,  1],
      /* 7*/ [15,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  1,  1,  0,  0,  0,  0,  0,  1,  1,  1,  0,  0,  0,  0,  0,  0, 36, 36, 36,  0,  1,  1,  1],
      /* 8*/ [ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -4, -2, -4, -2,  0,  0, -2, -4,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 36, 36, 36,  0,  0,  0,  1],
      /* 9*/ [ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  0,  0,  1,  1,  1,  1,  1,  1,  0,  0,  0],
      /*10*/ [ 0, -4,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 39,  0],
      /*11*/ [ 0, -3,  0,  0,  0,  0,  0,  0,  0, 14,  0,  0, 14,  0,  0, 14,  0,  0, 14,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 37,  0],
      /*12*/ [ 0, -2,  0,  0,  0,  0,  0,  0,  0,  0,  0, -2, -3, -4, -2, -3, -4, -2, -3, -4,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 38,  0],
      /*13*/ [ 0, -4,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 37,  0],
      /*14*/ [ 0, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 32, 32,  1,  0,  0,  1,  1],
      /*15*/ [ 0, -2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -4,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -2,  0,  0,  0, 32, 32,  1,  0, -2,  1,  1],
      /*16*/ [ 6,  9,  0,  0,  0,  0, -3,  0,  0, 40, 40,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -2,  0,  0,  0, 32, 32,  1, -4,  0,  1,  1],
      /*17*/ [ 2,  0,  0,  0,  0,  0, 13,  0,  0, 40, 40,  0,  0,  0,  0,  1,  1,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, -2,  0,  0,  0, 32, 32,  1,  0, -3,  1, -3],
      /*18*/ [ 2,  0, 14,  0,  0,  0,  0,  5,  6,  6,  6,  9,  0,  0,  1,  0,  0,  0, -2,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  0,  1, 32, 32,  1, -2,  0,  1, -3],
      /*19*/ [ 2,  0,  0,  0, -2,  0,  0,  0,  2, -5,  0,  0,  0,  1,  0,  0,  0,  0, 32, 32,  0,  1,  0,  0,  0,  1,  0,  0,  0,  0,  1,  0,  1, 32, 32,  1,  0, -4,  1, -3],
      /*20*/ [ 2,  0,  0,  0, 14,  0,  0,  0,  2, -4,  0,  0,  0,  0,  0,  0,  0,  0, 32, 32,  0,  1,  0,  0,  0,  1,  0,  0,  0,  0,  1,  0,  1, 32, 32,  1,  0,  0,  1,  1],
      /*21*/ [ 2,  0,  0,  0,  0,  0, 35,  0,  2, -3,  0,  0,  0,  0,  0,  0, 34,  0, 32, 32,  0,  1,  0,  0,  0,  1,  0,  0,  0,  0,  1,  0,  1,  1,  1,  1,  0,  0,  1,  1],
      /*22*/ [ 2, 33, 33, -1,  0,  0, 35, -4,  2, -2,  0,  0,  0,  0,  0,  0, 34,  0, 32, 32,  0,  1,  0,  0,  0,  1,  0,  0,  0, -3,  1,  0,  0,  0,  0,  0,  0,  0,  1,  1],
      /*23*/ [ 2, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 21,  0,  0, 18, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19]
      
      ];
      
      
      // Procedurally generate tile map data - stored in row major format
      /*
      let mapImage = [];
      
      for (let i=0; i<12; ++i) { // Map height iteration
        
        mapImage[i] = [];
        
        for (let j=0; j<32; ++j) { // Map width iteration
          
          // Kernel
          
          // 0 indicates an empty map tile
          mapImage[i][j] = (Math.random() < 0.1) ? 1 : 0;
        }
      }
      
      // Add floor
      for (let j=0; j<32; ++j) {
      
        mapImage[11][j] = 2;
      }    
      */
      
      return mapImage;
      
    })(),
    
    colliders : [
    
    // Note: Collider handler functions are referentially transparent just as state transition evaluation functions are.
    // Note 2: objA and objB can be strings or arrays of strings where multiple objects require the same behaviour.
    // Note 3: objA (or elements in array) map to the first parameter and objB (or elements in the array) map to the second parameter.  ALL objects with the associated colliderType must implement the methods / interfaces used in the collider handler.
    
    { 
      objA : 'player',
      objB : 'enemy',
      handler : function(player, enemy, env, tDelta) {
        
        player.setStrength(Math.max(player.strength() - enemy.damage(), 0));
      }
    },
    
    {
      objA : 'player',
      objB : 'pickup',
      handler : function(player, pickup, env, tDelta) {
        
        pickup.type().handler()(player);
        pickup.setCollected();
      }
    },
    
    {
      objA : 'player',
      objB : 'platform',
      handler : function(player, platform, env, tDelta) {
        
        let supports = env.pair.collision.supports;
        let dist = Game.Model.Contact.calcContactDeviation(supports);

        if (player.ContactProfile.calcContactProfile(player.position(), supports, dist) === Game.Model.Contact.bottom) {

          // Collision indicates we're on the platform so bind to the platform
          platform.Binding.bindObject(player);
        }
      }
    },
    
    {
      objA : 'player',
      objB : 'platform',
      collisionEnd : true, // collider called when collision ends, not starts
      handler : function(player, platform, env, tDelta) {
        
        if (platform.Binding.hasBoundObject(player)) {

          platform.Binding.unbindObject(player);
        }
      }
    },
    
    {
       objA : 'enemy',
       objB : ['bullet', 'grenade'],
       handler : function(enemy, projectile, env, tDelta) {
         
         console.log('enemy hit!');
         enemy.updateStrength(-projectile.model().damage);
         projectile.model().owner.addPoints(enemy.hitValue() * projectile.model().damage);
         projectile.setHitTarget();
       }
    },
    
    {
       objA : 'static_tile',
       objB : 'bullet',
       handler : function(tile, projectile, env, tDelta) {
         
         projectile.setHitTarget();
       }
    }
    
    ],
    
    // *** MAIN PLAYER SETUP ***
    players : {
      
      factory : Game.Model.Player,
      
      model : {
        
        anim_id : 'player',
        size : { width : 32, height : 32 },
        colliderType : 'player',
        collisionFilter : Game.Model.CollisionFilter.Player,
        initState : 'Standing',
        boundingVolumeScale : { width : 0.5, height : 0.9375 },
        numLives : 1
      }
    },
    
    endGameEvalFunc : function(player) {
    
      return player.position().y > 800 || player.strength() <= 0 || player.score()==60;
    },
    
    staticElements : [
    ],
    
    // The dynamicElements array contains NPCs and moving platforms
    // *** ADD YOUR OWN DYNAMIC ELEMENTS BASED ON YOUR LEVEL DESIGN.  HERE YOU SPECIFY THE COORDINATES OF WHERE YOU WANT THE ITEMS TO GO - SINCE THEY MOVE AROUND THEY'RE NOT TIED TO THE TILE ARRAY! ***
    dynamicElements : [
    
    { factory : Game.Model.NPC, // Type determines actual character / behaviour
      model : {
        anim_id : 'evil_snowman', // id determines animation sequence
        pos : { x : 384, y : 704},
        size : { width : 64, height : 64 },
        colliderType : 'enemy',
        collisionFilter : Game.Model.CollisionFilter.NPC,
        initState : 'Alive'
      } },
    
    { factory : Game.Model.NPC,
      model : {
        anim_id : 'evil_snowman',
        pos : { x : 496, y : 336},
        size : { width : 32, height : 32 }, 
        colliderType : 'enemy',
        collisionFilter : Game.Model.CollisionFilter.NPC,
        initState : 'Alive'
      } },
      
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        collisionFilter : Game.Model.CollisionFilter.Platform,
        initState : 'Default',
        delegate : Game.Delegates.Rotator,
        delegate_model : {
        
          origin : { x : 200, y : 100 },
          radius : 128,
          theta : 0, // determines where the platform starts
          speed : 90, // theta delta per second
          thetaDirection : -1 // determines the change of theta
        }
      } },
    
    
      { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 96, y : 496}, // for initial frame must match params below!!!!
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        collisionFilter : Game.Model.CollisionFilter.Platform,
        initState : 'Default',
        delegate : Game.Delegates.Oscillator,
        delegate_model : {
        
          origin : { x : 96, y : 356 }, // pos when sin(theta) = 0
          direction : { x : 0.0, y : 1.0 },
          range : 140.0, // oscillate over origin +- range
          speed : 90.0, // theta delta per second
          theta : 90.0, // determines where in the range the platform starts
          thetaDirection : -1 // determines the change of theta
        }
      } },
      
      { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 496, y : 464}, // for initial frame must match params below!!!!
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        collisionFilter : Game.Model.CollisionFilter.Platform,
        initState : 'Default',
        delegate : Game.Delegates.Oscillator,
        delegate_model : {
        
          origin : { x : 496, y : 464 }, // pos when sin(theta) = 0
          direction : { x : 1.0, y : 0.0 },
          range : 112.0, // oscillate over origin +- range
          speed : 90.0, // theta delta per second
          theta : 0.0, // determines where in the range the platform starts
          thetaDirection : 1 // determines the change of theta
        }
      } }
      
      
    /*{ factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        collisionFilter : Game.Model.CollisionFilter.Platform,
        initState : 'Default',
        delegate : Game.Delegates.Cycler,
        delegate_model : {
        
          origin : { x : 96, y : 216 },
          direction : { x : 0, y : 280 },
          speed : 0.20,
          t : 1.0,
          tDirection : -1.0
        }
      } }*/
      
    
      
    
      
    /*{ factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Rotator',
        state_config : {
        
          origin : { x : 200, y : 100 },
          radius : 128,
          initTheta : 0, // determines where the platform starts
          speed : 90, // theta delta per second
          direction : -1 // determines the change of theta
        }
      } }
      
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Rotator',
        state_config : {
        
          origin : { x : 200, y : 100 },
          radius : 128,
          initTheta : 180, // determines where the platform starts
          speed : 90, // theta delta per second
          direction : -1 // determines the change of theta
        }
      } }*/
      
   /* { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 380, y : 130},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Oscillator',
        state_config : {
        
          range : { x : 50, y : 0 },
          speed : 180, // theta delta per second
          initTheta : 0, // determines where in the range the platform starts
          direction : -1 // determines the change of theta
        }
      } }
    
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 210, y : 130},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Oscillator',
        state_config : {
        
          range : { x : 50, y : 0 },
          speed : 180, // theta delta per second
          initTheta : 0, // determines where in the range the platform starts
          direction : 1 // determines the change of theta
        }
      } }*/
      
    /*{ factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 300, y : 130},
        size : { width : 32, height : 16 },
        colliderType : 'platform',
        initState : 'Cycle',
        state_config : {
        
          type : 'horizontal',
          speed : 16,
          direction : 'right',
          range : { min : 50, max : 300 }
        }
      } }*/
    
    ]
    
  };
  
  
  return module;
  
})((Game.Levels || {}));