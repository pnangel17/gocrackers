
'use strict';

//
// Engine configuration
//

Game = (function(module) {

  module.config = {
    
    playerName : 'Player 1',

    player_mass : 18,
    player_move_speed : 128,
    player_top_speed : 256,
    player_jump_speed : -3.1,
    player_friction : 0.1,
    player_sprite_scale : 1,

    player_controls : { moveLeft : 'LEFT', moveRight : 'RIGHT', jump : 'SPACE', fire : 'Z', fire2 : 'X' },
    
    pickup_sprite_scale : 0.75, // Bigger number = larger sprite!
    pickup_time_delay : 5, // Delay (in seconds) between pickups appearing

    projectile_default_sprite_scale : 1,
    
    frictionCoeff : 0.025,
    
    default_npc_mass : 18,
    default_npc_sprite_scale : 1,
    default_npc_hit_value : 10,
    default_npc_strength : 100,
    default_npc_damage : 10,
    
    default_platform_mass : 20,
    default_platform_sprite_scale : 1,
    
    show_bounding_volume : false,
    
    world_gravity : { x : 0, y : 0.8 }
  };
  
  
  return module;
  
})((window.Game || {}));

