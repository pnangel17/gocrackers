'use strict';

// Render level modes - these control how your background images are rendered in relation to the main webpage canvas and camera viewport size.

let RenderLevelImageMode = Object.freeze({
  "Normal"    : 1,
  "Tile"      : 2,
  "Fill"      : 3   // Fit to world coordinates
});


// This module contains the behaviours for the moving platforms
Game.Delegates = (function(module) {
  
  // The oscillator platform moves up / down or from size-to-side.
  module.Oscillator = function(model) {
    
    let delegate = {};
    
    let ps = {
      
      origin : { x : model.origin.x, y : model.origin.y },
      
      // Unit-length direction vector for the oscillation movement
      direction : { x : model.direction.x, y : model.direction.y },
      
      range : model.range,
      speed : model.speed, // theta delta per second
      
      theta : (model.theta || 0), // optional - determines where in the range the platform starts    
      thetaDirection : (model.thetaDirection || 1) // optional - determines the change of theta
    };
    
    delegate.update = function(host, env, tDelta) {
      
      ps.theta += ps.speed * tDelta * ps.thetaDirection;
      
      // Ensure sign lies in [0, 360) interval.
      ps.theta = ps.theta % 360;
      
      if (ps.theta < 0) {
        
        ps.theta += 360;
      }
      
      // Calculate and return new position
      let x = Math.sin(Game.Math.Radian * ps.theta) * ps.range * ps.direction.x + ps.origin.x;
      let y = Math.sin(Game.Math.Radian * ps.theta) * ps.range * ps.direction.y + ps.origin.y;
      
      return { position : { x : x, y : y } };
    }
    
    return delegate;
  };
  
  // The cycler platform moves in a single direction and jump back to the start when it reaches the end of its range.
  module.Cycler = function(model) {
  
    let delegate = {};
    
    let ps = {
      
      origin : { x : model.origin.x, y : model.origin.y },
      direction : { x : model.direction.x, y : model.direction.y },
      
      speed : model.speed,
      t : (model.t || 0),
      tDirection : (model.tDirection || 1)
    };
    
    delegate.update = function(host, env, tDelta) {
      
      ps.t += ps.speed * tDelta * ps.tDirection;
      
      if (ps.t > 1.0) {
        
        ps.t -= 1.0;
      }
      else if (ps.t < 0.0) {
        
        ps.t += 1.0;
      }
      
      let x = ps.origin.x + ps.direction.x * ps.t;
      let y = ps.origin.y + ps.direction.y * ps.t;
      
      return { position : { x : x, y : y } };
    }
    
    return delegate;
  }
  
  // The rotator platform moves in a circular motion
  module.Rotator = function(model) {
    
    let delegate = {};
    
    // Setup private state and initialise
    let ps = {
      
      origin : { x : model.origin.x, y : model.origin.y },
      radius : model.radius,
      speed : model.speed,
      theta : (model.theta || 0),
      thetaDirection : (model.thetaDirection || 1)
    };
    
    delegate.update = function(host, env, tDelta) {
      
      ps.theta += ps.speed * tDelta * ps.thetaDirection;
      
      // Ensure sign lies in [0, 360) interval.
      ps.theta = ps.theta % 360;
      
      if (ps.theta < 0) {
        
        ps.theta += 360;
      }
      
      // Calculate and return new position
      let x = Math.cos(Game.Math.Radian * ps.theta) * ps.radius + ps.origin.x;
      let y = Math.sin(Game.Math.Radian * ps.theta) * ps.radius + ps.origin.y;
        
      return { position : { x : x, y : y } };
    }
    
    return delegate;
  };
  
  return module;
  
})((Game.Delegates || {}));


Game.StaticElementTypes = (function(types){
  
  types[32] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Tio_Aimar//tree01.png',
    boundingVolumeScale : { width : 0.9, height : 1.0 }
  });
  
  types[33] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Tio_Aimar//igloo.png'
  });
  
  types[34] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Kenney//tree02.png'
  });
  
  types[35] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Kenney//tree03.png'
  });
  
  types[36] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Kenney//caneRedSmall.png'
  });
  
  types[37] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Kenney//caneRed.png'
  });
  
  types[38] = Game.Model.StaticElement.Type({ // alternate 37, 38 to avoid single model with stretched stripes
    
    spriteURI : 'Assets//Images//Kenney//caneRed.png'
  });
  
  types[39] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Kenney//caneRedTop.png'
  });
  
  types[40] = Game.Model.StaticElement.Type({
    
    spriteURI : 'Assets//Images//Tio_Aimar//snowman.png'
  });
  
  
  
  return types;
  
})(Game.StaticElementTypes || []);


// The Pickup types module defines the types of pickups you find.  Types are given a unique id just like sprites above.  So the points pickup has an id of 10 for example.
Game.PickupTypes = (function(types) {
  
  types[-2] = Game.Model.Pickup.Type( {
    
    // Types MUST have certain properties set...
    
    // spriteURI specifies the sprite image file to show
    spriteURI : 'Assets//Images//red_cracker.png',
    
    // The handler function determines what happens when a collector (the player for example) pickups up the pickup
    handler : function(collector) {
    
      // In this example we execute the function addPoints on the collector to given them 50 points.
      collector.addPoints(1);
    }
    
  });
  
  types[-3] = Game.Model.Pickup.Type( {
    
    // Types MUST have certain properties set...
    
    // spriteURI specifies the sprite image file to show
    spriteURI : 'Assets//Images//blue_cracker.png',
    
    // The handler function determines what happens when a collector (the player for example) pickups up the pickup
    handler : function(collector) {
    
      // In this example we execute the function addPoints on the collector to given them 50 points.
      collector.addPoints(1);
    }
    
  });
  
  types[-4] = Game.Model.Pickup.Type( {
    
    // Types MUST have certain properties set...
    
    // spriteURI specifies the sprite image file to show
    spriteURI : 'Assets//Images//green_cracker.png',
    
    // The handler function determines what happens when a collector (the player for example) pickups up the pickup
    handler : function(collector) {
    
      // In this example we execute the function addPoints on the collector to given them 50 points.
      collector.addPoints(1);
    }
    
  });
  
  
  // Health pickup
  types[-5] = Game.Model.Pickup.Type( {
    
    spriteURI : 'Assets//Images//looneybits//green_pickup.png',
    handler : function(collector) {
    
      collector.addStrength(20);
    }
  });

  // Extra life pickup
  types[-6] = Game.Model.Pickup.Type( {
    
    spriteURI : 'Assets//Images//looneybits//blue_pickup.png',
    handler : function(collector) {
    
      collector.addExtraLife();
    }
  });
  
  
  return types;
  
})(Game.PickupTypes || []);


// Quick note:  Difference between colliderType and collisionFilter:
// collisionFilter is used by Matter.js to determine WHAT can colliderType
// colliderType determines (at the game engine level) which colliders get called when a collision occurs - so determines WHAT HAPPENS when a collision occurs.
Game.ProjectileTypes = (function(types) {

  types['Bullet'] = Game.Model.Projectile.Type({
    
    model : function(owner, env, tDelta, overrideModel) {
      
      return {
        
        owner : owner,
        direction : owner.direction(),
        range : 1000,
        speed : 300,
        damage : 100,
        colliderType : 'bullet',
        bodyProperties : {
          
          mass : 0.002,
          inverseMass : 1.0 / 0.002,
          isStatic : true,
          isSensor : true,
          collisionFilter : overrideModel.collisionFilter || Game.Model.CollisionFilter.Projectile
        },
        scale : 1.0,
        boundingVolumeScale : 0.8,
        distanceTravelled : 0
      };
    },
    
    sprite : Game.Graphics.Sprite({imageURL: 'Assets/Images/bullet01.png'}),
    
    matterBody : function(model, sprite) {
      
      let pos = model.owner.position();
      let radius = sprite.image().width * model.scale * model.boundingVolumeScale;
      
      return Matter.Bodies.circle(pos.x, pos.y, radius, model.bodyProperties);
    },
    
    states : {
        
      'Fired' : function(params) {
        
        let state = Game.Model.State(params);
        
        let state_ps = {
          
          direction : params.model.direction,
          speed : params.model.speed,
        };
        
        state.update = function(hostEntity, env, tDelta) {
                    
          let body = hostEntity.body();
          
          let x_ = body.position.x + state_ps.speed * state_ps.direction * tDelta;
          let dx = x_ - body.position.x;
          
          //body.positionPrev = { x : body.position.x, y : body.position.y };
          
          Matter.Body.setVelocity(body, {x : dx, y : 0});
          Matter.Body.setPosition(body, {x : x_, y : body.position.y});
          
          hostEntity.model().distanceTravelled += Math.abs(dx);
          
          return true;
        }
        
        return state;        
      },
      
      'End' : function(params) {
        
        let state = Game.Model.State(params);
              
        state.update = function(hostEntity, env, tDelta) {
          
          return false;
        }
        
        return state;
      }
    },
    
    transitions : [
    
      { source : 'Fired',
        target : 'End',
        cond : function(hostEntity, env, tDelta) {
          
          return (hostEntity.model().distanceTravelled >= hostEntity.model().range) || hostEntity.hitTarget()==true;
        }
      }
    ]
    
  });
  
  
  types['Grenade'] = Game.Model.Projectile.Type({
    
    model : function(owner, env, tDelta, overrideModel) {
      
      return {
        
        owner : owner,
        force : { x : owner.direction() * 0.225, y : -0.5 },
        timer : 5, // time before 'exploding'
        damage : 400,
        colliderType : 'grenade',
        bodyProperties : {
          
          mass : 15.0,
          inverseMass : 1.0 / 15.0,
          frictionAir : 0.01,
          collisionFilter : overrideModel.collisionFilter || Game.Model.CollisionFilter.Projectile,
          restitution : 0.9
        },
        scale : 1.0,
        boundingVolumeScale : 0.8
      };
    },
    
    sprite : Game.Graphics.Sprite({imageURL: 'Assets/Images/bullet01.png'}),
    
    matterBody : function(model, sprite) {
    
      let pos = model.owner.position();
      let radius = sprite.image().width * model.scale * model.boundingVolumeScale;
      
      return Matter.Bodies.circle(pos.x, pos.y, radius, model.bodyProperties);
    },
    
    states : {
        
      'Throw' : function(params) {
        
        let state = Game.Model.State(params);
                
        // Capture relevant config info from params here
        let state_ps = {
          
          initPos : params.model.owner.position(),
          initForce : params.model.force
        };
        
        state.enter = function(hostEntity, env, tDelta) {
          
          Matter.Body.applyForce(hostEntity.body(), state_ps.initPos, state_ps.initForce);
        }
        
        state.update = function(hostEntity, env, tDelta) {
          
          hostEntity.model().timer -= tDelta;
          return true;
        }
        
        return state;        
      },
      
      'End' : function(params) {
        
        let state = Game.Model.State(params);
      
        state.update = function(hostEntity, env, tDelta) {
          
          return false;
        }
        
        return state;
      }
    },
    
    transitions : [
    
      // Transition for when grenade's time is up!
      { source : 'Throw',
        target : 'End',
        cond : function(hostEntity, env, tDelta) {
          
          return hostEntity.model().timer <= 0 || hostEntity.hitTarget()==true;
        }
      }    
    ]
  });
  
  
  return types;
  
})(Game.ProjectileTypes || []);
