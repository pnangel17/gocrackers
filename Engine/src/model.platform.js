'use strict';

// Kinematic platform model

Game.Model = (function(module) {
  
  // Array-based version of object / dictionary version for platform bindings
  module.PlatformBindingArray = function() {
    
    let self = {};
    
    let ps = {
      
      bindings : []
    };
    
    let bindObject = function(obj) {
      
      if (ps.bindings.indexOf(obj)==-1) {
              
        ps.bindings.push(obj);
      }
    }
    
    let unbindObject = function(obj) {
      
      let index = ps.bindings.indexOf(obj);
      
      if (index >= 0) {
              
        ps.bindings.splice(index, 1);
      }
    }
    
    let hasBoundObject = function(obj) {
    
      return ps.bindings.indexOf(obj) >= 0;
    }
    
    let update = function(T) {
      
      for (let obj of ps.bindings) {
        
        obj.PlatformBinding.update(T);
      }
    }
    
    // Public interface
    self.bindObject = bindObject;
    self.unbindObject = unbindObject;
    self.hasBoundObject = hasBoundObject;
    self.update = update;
    
    return self;
  };
  
  
  module.Platform = function(model, world) {
    
    let entity = Game.Model.Entity(model);
    
    let ps = {
    
      anim_id : model.anim_id,
      body : Matter.Bodies.rectangle(model.pos.x, model.pos.y, model.size.width, model.size.height, {
        isStatic : true,
        isSensor : false,
        collisionFilter : model.collisionFilter
      }),
      size : model.size,
      scale : (model.scale || Game.config.default_platform_sprite_scale),
      boundingVolumeScale : (model.boundingVolumeScale || 1.0),
      state_config : model.state_config // immutable
    };
    
    ps.body.hostObject = entity;
    Matter.World.add(world, ps.body);
    
    
    //{ Platform states
    
    // Default state does nothing to the platform - but does delegate update functionality when defined.
    let DefaultState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      let state_ps = {
        
        delegate : params.delegate
      };
      
      state.enter = function(hostEntity, env, tDelta) {
        
        entity.setCurrentAnimationSequence(Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Moving']})); // Note: We're not tied to state names here!
      }
      
      state.update = function(hostEntity, env, tDelta) {
        
        entity.currentAnimationSequence().updateFrame(tDelta);
        
        // Handle delegation
        if (state_ps.delegate) {
          
          let newState = state_ps.delegate.update(entity, env, tDelta);
          
          if (newState) {
            
            let dx = newState.position.x - ps.body.position.x;
            let dy = newState.position.y - ps.body.position.y;
            
            Matter.Body.setPosition(ps.body, {x : newState.position.x, y : newState.position.y});
            
            // Note: Don't update velocity for platforms - so does not perform as a normal kinematic body (https://github.com/liabru/matter-js/issues/610) to we can more easily maintain states and physical values of bound objects.
            //Matter.Body.setVelocity(ps.body, {x : dx, y : dy});
            
            // Update all bound objects / bodies
            entity.Binding.update({x : dx, y : dy});
          }
        }
        
        return true;
      }
            
      return state;
    }
    
    // EndState typically terminates a host character / entity.  This provides a convinient realisation of state terminator in a state diagram.
    let EndState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      state.update = function(hostEntity, env, tDelta) {
        
        return false;
      }
      
      return state;
    }
    
    //}
    
    
    
    //{ State-graph and transition setup
    
    entity.addState('Default', DefaultState( { delegate : model.delegate(model.delegate_model) } ));
    entity.addState('End', EndState());
    
    
    /*
    // test - provide entire state - depricated because ps.state_config would need to be a superset of state attributes.  Better solution would be to allow new states + transitions to be added to graph where default platform behaviour isn't wanted.
    let behaviour = model.state( { host_entity : entity, host_state : ps } );
    let behaviourName = behaviour.name;
    entity.addState(behaviourName, behaviour);
    */
    
    //}
    
    
    //{ Private API
    
    let draw = function(context, canvas) {
      
      // Draw main image rect
      context.save();
      
      context.translate(ps.body.position.x, ps.body.position.y);
      context.rotate(ps.body.angle);
      
      let anim = entity.currentAnimationSequence();
      
      if (anim !== null) {
        
        anim.drawCurrentFrame(ps.size, ps.scale, context);
        
      } else {
      
        context.fillStyle = 'rgb(0, 255, 0)';
        context.fillRect(-ps.size.width/2, -ps.size.height/2, ps.size.width, ps.size.height);
      }
      
      context.restore();
      
      if (Game.config.show_bounding_volume) {
      
        // Draw bounding volume (no model transform needed since already in world coords)
        
        let vertices = ps.body.vertices;
        
        context.beginPath();

        context.moveTo(vertices[0].x, vertices[0].y);
        
        for (let i = 1; i < vertices.length; ++i) {
        
          context.lineTo(vertices[i].x, vertices[i].y);
        }
        
        context.lineTo(vertices[0].x, vertices[0].y);
        
        context.lineWidth = 1;
        context.strokeStyle = '#FFF';
        context.stroke();
        
              
        // Draw player contact profile (again, already in world coords so no transform needed)
        
        context.beginPath();
        
        if ((ps.contactProfile & Game.Model.Contact.top) == Game.Model.Contact.top) {
          
          context.moveTo(ps.body.position.x - (ps.size.width / 2), ps.body.position.y - (ps.size.height / 2));
          context.lineTo(ps.body.position.x + (ps.size.width / 2), ps.body.position.y - (ps.size.height / 2));
        }
        if ((ps.contactProfile & Game.Model.Contact.bottom) == Game.Model.Contact.bottom) {
          
          context.moveTo(ps.body.position.x - (ps.size.width / 2), ps.body.position.y + (ps.size.height / 2));
          context.lineTo(ps.body.position.x + (ps.size.width / 2), ps.body.position.y + (ps.size.height / 2));
        }
        if ((ps.contactProfile & Game.Model.Contact.left) == Game.Model.Contact.left) {
          
          context.moveTo(ps.body.position.x - (ps.size.width / 2), ps.body.position.y - (ps.size.height / 2));
          context.lineTo(ps.body.position.x - (ps.size.width / 2), ps.body.position.y + (ps.size.height / 2));
        }
        if ((ps.contactProfile & Game.Model.Contact.right) == Game.Model.Contact.right) {
          
          context.moveTo(ps.body.position.x + (ps.size.width / 2), ps.body.position.y - (ps.size.height / 2));
          context.lineTo(ps.body.position.x + (ps.size.width / 2), ps.body.position.y + (ps.size.height / 2));
        }
        
        context.lineWidth = 8;
        context.strokeStyle = '#FFF';
        context.stroke();
      }
    }
    
    
    // Accessor methods
    
    let anim_id = function() {
      
      return ps.anim_id;
    }
    
    let body = function() {
      
      return ps.body;
    }
    
    //}
    
    
    
    //{ Pubic interface
    
    entity.draw = draw;
    entity.anim_id = anim_id;
    entity.body = body;    
    
    entity.Binding = Game.Model.PlatformBindingArray();
    entity.CollisionInterface = Game.Model.CollisionInterface(model.colliderType);
    
    //}
    
    
    entity.setCurrentState(model.initState, model.env, model.tDelta);
    
    return entity;
  }
  
  return module;

})((Game.Model || {}));
