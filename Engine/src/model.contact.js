'use strict';

// Contact model

Game.Model = (function(module) {
  
  module.CollisionFilter = {
    
    StaticScenery : {
      
      group :     0,
      category :  0b0000000000000001,
      mask :      0b0000000000111110
    },
    
    Player : {
      
      group :     0,
      category :  0b0000000000000010,
      mask :      0b0000000000111011
    },
    
    Projectile : {
      
      group :     0,
      category :  0b0000000000000100,
      mask :      0b0000000000110101
    },
    
    Pickup : {
      
      group :     0,
      category :  0b0000000000001000,
      mask :      0b0000000000110110
    },
    
    // Collision filter for ANY NPC
    NPC : {
      
      group :     0,
      category :  0b0000000000010000,
      mask :      0b0000000000100111
    },
    
    Platform : {
      
      group :     0,
      category :  0b0000000000100000,
      mask :      0b0000000000111111
    }
    
  };
  
  
  module.CollisionInterface = function(typeName = 'null') {
    
    self = {};
    
    // Private state
    
    let ps = {
      
      type : typeName
    };
    
    // Public interface
    
    self.type = function() {
        
        return ps.type;
    };
    
    return self;
  };
  
  
  module.Contact = {
    
    bottom : 0b0001,
    top    : 0b0010,
    left   : 0b0100,
    right  : 0b1000,
    
    // For a given 'supports' structure, calculate mean x contact position and standard deviation and mean y contact position and standard deviation
    calcContactDeviation : function(supports) {
      
      let mX = supports.reduce(function(sum, val) { return sum + val.x; }, 0) / supports.length;
      
      let mY = supports.reduce(function(sum, val) { return sum + val.y; }, 0) / supports.length;
      
      return {
        
        meanX : mX,
        meanY : mY,
        
        dx : supports.reduce(function(sum, val) { return sum + Math.pow(val.x - mX, 2); }, 0) / supports.length,
        
        dy : supports.reduce(function(sum, val) { return sum + Math.pow(val.y - mY, 2); }, 0) / supports.length
      };
    }
  };

  
  module.ContactProfileInterface = function(params = {}) {
    
    let self = {};
    
    // Private state
    let ps = {
      
      contactProfile : 0
    };
    
    // Private API
    
    let contactProfile = function() {
      
      return ps.contactProfile;
    }
    
    // Return the contact profile flag given the supporting contact points between two objects.  This does not affect the state of the contact profile for the host object on which it's called.
    let calcContactProfile = function(position, supports, dist) {
              
      if (supports.length >= 2) {
        
        if (dist.dx >= dist.dy) {
          
          // Horizontally oriented contact
          if (dist.meanY < position.y) {
            
            return Game.Model.Contact.top;
          }
          else {
            
            return Game.Model.Contact.bottom;
          }
        }
        else {
                    
          if (dist.meanX < position.x) {
            
            return Game.Model.Contact.left;
          }
          else {
            
            return Game.Model.Contact.right;
          }
        }
        
      } else { // must be 1 - special corner contact case
      
        if (supports[0].y < position.y) {
          
          return Game.Model.Contact.top;
        }
        else {
          
          return Game.Model.Contact.bottom;
        }            
      }
    }
    
    // Update contact profile bit fields.  position refers to the position of the host object / body on which the contact profile is being updated.
    let updateContactProfile = function(position, supports, dist) {
      
      ps.contactProfile = ps.contactProfile | calcContactProfile(position, supports, dist);
    }
    
    let resetContactProfile = function() {
      
      ps.contactProfile = 0;
    }
    
    // Public interface
    self.contactProfile = contactProfile;
    self.calcContactProfile = calcContactProfile;
    self.updateContactProfile = updateContactProfile;
    self.resetContactProfile = resetContactProfile;
    
    return self;
  }
  
  
  return module;
  
})((Game.Model || {}));