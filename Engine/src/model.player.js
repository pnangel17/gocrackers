'use strict';

// Main player entity

Game.Model = (function(module) {
  
  module.FacingDirection = (module.FacingDirection || { Left : -1, Right : 1 });
  
  module.Player = function(model, world) {
  
    let entity = Game.Model.Entity(model);
    
    //{ Private state
    
    let boundingVolumeScale = (model.boundingVolumeScale || { width : 1.0, height : 1.0 })
    
    let ps = {
    
      anim_id : model.anim_id, // Host (game) object id - NOT physics engine id
      body : Matter.Bodies.rectangle(model.pos.x, model.pos.y, model.size.width * boundingVolumeScale.width, model.size.height * boundingVolumeScale.height, {
        mass : (model.mass || Game.config.player_mass),
        inverseMass : 1.0 / (model.mass || Game.config.player_mass),
        inertia : Number.POSITIVE_INFINITY,
        collisionFilter : model.collisionFilter
      }),
      boundingVolumeScale : boundingVolumeScale,
      size : model.size,
      scale : (model.scale || Game.config.player_sprite_scale),
      
      direction : (model.direction || Game.Model.FacingDirection.Right),
      
      strength : (model.strength || 100),
      numLives : (model.numLives || 3),
      score : 0
    };
    
    ps.body.hostObject = entity;
    ps.body.playerFlag = true;
    Matter.World.add(world, ps.body);
    
    //}
    
    
    //{ Player states

    let StandingState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      let state_ps = {
      
        standLeft : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Standing_left']}),
        
        standRight : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Standing_right']})
      };
      
      // Public interface
      
      state.enter = function(hostEntity, env, tDelta) {
        
        if (ps.direction==Game.Model.FacingDirection.Right) {
        
          entity.setCurrentAnimationSequence(state_ps.standRight.reset());
        }
        else {
          
          entity.setCurrentAnimationSequence(state_ps.standLeft.reset());
        }
      }
      
      state.update = function(hostEntity, env, tDelta) {
        
        /*
        if (env.stage.keyboard().isPressed(Game.config.player_controls.fire)) {
          
          let p = Game.Model.Projectile.Instance(Game.ProjectileTypes['Bullet'], hostEntity, env, tDelta, {}, 'Fired');
          
          env.stage.projectiles().push(p);
        }
        
        if (env.stage.keyboard().isPressed(Game.config.player_controls.fire2)) {
          
          let p = Game.Model.Projectile.Instance(Game.ProjectileTypes['Grenade'], hostEntity, env, tDelta, {}, 'Throw');
          
          env.stage.projectiles().push(p);
        }
        */
        
        entity.currentAnimationSequence().updateFrame(tDelta);
        return true;
      }
      
      // Note: no need to override exit - does nothing for this state
      
      return state;
    }
    
    let WalkingState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      let state_ps = {
      
        walkingLeft : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Walking_left']}),
        
        walkingRight : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Walking_right']})
      };
      
      // Public interface
      
      state.enter = function(hostEntity, env, tDelta) {
        
        if (ps.direction==Game.Model.FacingDirection.Right) {
          
          entity.setCurrentAnimationSequence(state_ps.walkingRight.reset());
        }
        else {
          
          entity.setCurrentAnimationSequence(state_ps.walkingLeft.reset());
        }
      }
      
      state.update = function(hostEntity, env, tDelta) {
        
        if (env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft)) {
          
          Matter.Body.translate(ps.body, { x : -Game.config.player_move_speed * tDelta, y : 0 } );
          
          // Manage change in 'sub-state' ie. change direction
          if (ps.direction!=Game.Model.FacingDirection.Left) {
            
            ps.direction = Game.Model.FacingDirection.Left;
            entity.setCurrentAnimationSequence(state_ps.walkingLeft.reset());
          }
        }
        else if (env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)) {
          
          Matter.Body.translate(ps.body, { x : Game.config.player_move_speed * tDelta, y : 0 } );
          
          if (ps.direction!=Game.Model.FacingDirection.Right) {
            
            ps.direction = Game.Model.FacingDirection.Right;
            entity.setCurrentAnimationSequence(state_ps.walkingRight.reset());
          }
        }
        
        entity.currentAnimationSequence().updateFrame(tDelta);        
        
        return true;
      }
             
      return state;
    }
    
    let JumpingState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      let state_ps = {
        
        jumpingLeft : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Jumping_left']}),
        
        jumpingRight : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Jumping_right']})
      };
      
      // Public interface
      
      state.enter = function(hostEntity, env, tDelta) {
        
        if (ps.direction==Game.Model.FacingDirection.Right) {
          
          entity.setCurrentAnimationSequence(state_ps.jumpingRight.reset());
        }
        else {
          
          entity.setCurrentAnimationSequence(state_ps.jumpingLeft.reset());
        }
        
        ps.body.friction = 0; 
        Matter.Body.setVelocity(ps.body, { x : ps.body.velocity.x, y : Game.config.player_jump_speed});
        
      }
      
      state.update = function(hostEntity, env, tDelta) {
        
        if (env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft)) {
          
          Matter.Body.translate(ps.body, { x : -Game.config.player_move_speed * tDelta, y : 0 } );
          
          if (ps.direction!=Game.Model.FacingDirection.Left) {
            
            ps.direction = Game.Model.FacingDirection.Left;
            entity.setCurrentAnimationSequence(state_ps.jumpingLeft.reset());
          }
        }
        else if (env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)) {
          
          Matter.Body.translate(ps.body, { x : Game.config.player_move_speed * tDelta, y : 0 } );
          
          if (ps.direction!=Game.Model.FacingDirection.Right) {
            
            ps.direction = Game.Model.FacingDirection.Right;
            entity.setCurrentAnimationSequence(state_ps.jumpingRight.reset());
          }
        }
        
        entity.currentAnimationSequence().updateFrame(tDelta);
        
        return true;
      }
      
      state.exit = function(hostEntity, env, tDelta) {
      
        // Restore friction coefficient for body
        ps.body.friction = Game.config.player_friction;
      }
      
      return state;
    }
    
    let FallingState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      let state_ps = {
      
        fallingLeft : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Falling_left']}),
        
        fallingRight : Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Falling_right']})
      };
      
      // Public interface
      
      state.enter = function(hostEntity, env, tDelta) {
      
        if (ps.direction==Game.Model.FacingDirection.Right) {
          
          entity.setCurrentAnimationSequence(state_ps.fallingRight.reset());
        }
        else {
          
          entity.setCurrentAnimationSequence(state_ps.fallingLeft.reset());
        }
        
        ps.body.friction = 0;
      }
      
      state.update = function(hostEntity, env, tDelta) {
                
        if (env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft)) {
          
          Matter.Body.translate(ps.body, { x : -Game.config.player_move_speed * tDelta, y : 0 } );
          
          if (ps.direction!=Game.Model.FacingDirection.Left) {
            
            ps.direction = Game.Model.FacingDirection.Left;
            entity.setCurrentAnimationSequence(state_ps.fallingLeft.reset());
          }
        }
        else if (env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)) {
          
          Matter.Body.translate(ps.body, { x : Game.config.player_move_speed * tDelta, y : 0 } );
          
          if (ps.direction!=Game.Model.FacingDirection.Right) {
            
            ps.direction = Game.Model.FacingDirection.Right;
            entity.setCurrentAnimationSequence(state_ps.fallingRight.reset());
          }
        }
        
        entity.currentAnimationSequence().updateFrame(tDelta);
        
        return true;
      }
      
      state.exit = function(hostEntity, env, tDelta) {
              
        // Restore friction coefficient for body
        ps.body.friction = Game.config.player_friction;
      }
      
      return state;
    }
    
    // EndState typically terminates a host character / entity.  This provides a convinient realisation of state terminator in a state diagram.
    let EndState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      // Public interface
      
      state.update = function(hostEntity, env, tDelta) {
        
        console.log("Player Dead!");
        
        return false;
      }
      
      return state;
    }
  
    //}
    
    
    //{ State-graph and transition setup
    
    entity.addState('Standing', StandingState());
    entity.addState('Walking', WalkingState());
    entity.addState('Jumping', JumpingState());
    entity.addState('Falling', FallingState());
    entity.addState('End', EndState());
    
    
    // Standing->Walking
    entity.addTransition('Standing', 'Walking',
    
      function(hostEntity, env, tDelta) {
        
        return ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==Game.Model.Contact.bottom && (env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft) || env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)));
      }
    );
    
    // Walking->Standing
    entity.addTransition('Walking', 'Standing',
    
      function(hostEntity, env, tDelta) {
        
        return ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==Game.Model.Contact.bottom && (!env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft) && !env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)));
      }
    );
    
    // Standing->Jumping
    entity.addTransition('Standing', 'Jumping', 
    
      function(hostEntity, env, tDelta) {
        
        return env.stage.keyboard().isPressed(Game.config.player_controls.jump);
      }    
    );
    
    // Walking->Jumping
    entity.addTransition('Walking', 'Jumping',
    
      function(hostEntity, env, tDelta) {
        
        return env.stage.keyboard().isPressed(Game.config.player_controls.jump);
      }
    );
    
    // Standing->Falling
    entity.addTransition('Standing', 'Falling',
    
      function(hostEntity, env, tDelta) {
        
        return (entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==0;
      }
    );
    
    // Walking->Falling
    entity.addTransition('Walking', 'Falling',
    
      function(hostEntity, env, tDelta) {
        
        return (entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==0;
      }
    );
    
    // Jumping->Standing
    entity.addTransition('Jumping', 'Standing',
    
      function(hostEntity, env, tDelta) {
        
        return ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==Game.Model.Contact.bottom && (!env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft) && !env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)));
      }
    );
    
    // Jumping->Walking
    entity.addTransition('Jumping', 'Walking',
    
      function(hostEntity, env, tDelta) {
        
        return ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==Game.Model.Contact.bottom && (env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft) || env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)));
      }
    );
    
    // Jumping->Falling
    entity.addTransition('Jumping', 'Falling',
    
      function(hostEntity, env, tDelta) {
        
        let colliderMask = Game.Model.Contact.top /*| Game.Model.Contact.left | Game.Model.Contact.right*/ ;
        
        return (entity.ContactProfile.contactProfile() & colliderMask) != 0;
      }
    );
    
    // Falling->Standing
    entity.addTransition('Falling', 'Standing',
    
      function(hostEntity, env, tDelta) {
        
        return ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==Game.Model.Contact.bottom && (!env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft) && !env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)));
      }
    );
    
    // Falling->Walking
    entity.addTransition('Falling', 'Walking',
    
      function(hostEntity, env, tDelta) {
        
        return ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom)==Game.Model.Contact.bottom && (env.stage.keyboard().isPressed(Game.config.player_controls.moveLeft) || env.stage.keyboard().isPressed(Game.config.player_controls.moveRight)));
      }
    );
    
    
    // Note: Transition to dying state - End really used to completely terminate and delete in-game entities!
    entity.addTransition(null, 'End',
    
      function(hostEntity, env, tDelta) {
      
        return entity.strength() == 0;
      }
    );
    
    
    //}

    
    //{ Private API
    
    let draw = function(context, canvas) {
      
      // Draw main image rect
      context.save();
      
      context.translate(ps.body.position.x, ps.body.position.y);
      context.rotate(ps.body.angle);
      
      
      let anim = entity.currentAnimationSequence();
      
      if (anim !== null) {
        
        anim.drawCurrentFrame(ps.size, ps.scale, context);
        
      } else {
      
        context.fillStyle = 'rgb(0, 255, 0)';
        context.fillRect(-ps.size.width/2, -ps.size.height/2, ps.size.width, ps.size.height);
      }
      
      context.restore();
      
      if (Game.config.show_bounding_volume) {
       
        // Draw bounding volume (no model transform needed since already in world coords)
      
        let vertices = ps.body.vertices;
        
        context.beginPath();

        context.moveTo(vertices[0].x, vertices[0].y);
        
        for (let i = 1; i < vertices.length; ++i) {
        
          context.lineTo(vertices[i].x, vertices[i].y);
        }
        
        context.lineTo(vertices[0].x, vertices[0].y);
        
        context.lineWidth = 1;
        context.strokeStyle = '#FFF';
        context.stroke();
        
            
        // Draw player contact profile (again, already in world coords so no transform needed)
        
        context.beginPath();
        
        let w = ps.size.width * ps.boundingVolumeScale.width;
        let h = ps.size.height * ps.boundingVolumeScale.height;
        
        if ((entity.ContactProfile.contactProfile() & Game.Model.Contact.top) == Game.Model.Contact.top) {
          
          context.moveTo(ps.body.position.x - (w / 2), ps.body.position.y - (h / 2));
          context.lineTo(ps.body.position.x + (w / 2), ps.body.position.y - (h / 2));
        }
        if ((entity.ContactProfile.contactProfile() & Game.Model.Contact.bottom) == Game.Model.Contact.bottom) {
          
          context.moveTo(ps.body.position.x - (w / 2), ps.body.position.y + (h / 2));
          context.lineTo(ps.body.position.x + (w / 2), ps.body.position.y + (h / 2));
        }
        if ((entity.ContactProfile.contactProfile() & Game.Model.Contact.left) == Game.Model.Contact.left) {
          
          context.moveTo(ps.body.position.x - (w / 2), ps.body.position.y - (h / 2));
          context.lineTo(ps.body.position.x - (w / 2), ps.body.position.y + (h / 2));
        }
        if ((entity.ContactProfile.contactProfile() & Game.Model.Contact.right) == Game.Model.Contact.right) {
          
          context.moveTo(ps.body.position.x + (w / 2), ps.body.position.y - (h / 2));
          context.lineTo(ps.body.position.x + (w / 2), ps.body.position.y + (h / 2));
        }
      
        context.lineWidth = 4;
        context.strokeStyle = '#F00';
        context.stroke();
      }
      
    };
    
    
    // Accessor methods
    
    let anim_id = function() {
      
      return ps.anim_id;
    }
    
    let body = function() {
      
      return ps.body;
    }
    
    let currentState = function() {
      
      return ps.currentState;
    }
    
    let position = function() {
      
      return { x : ps.body.position.x, y : ps.body.position.y };
    }
    
    let direction = function() {
      
      return ps.direction; // Left / right facing
    }
    
    let score = function() { 
    
      return ps.score;
    }
    
    let strength = function() {
      
      return ps.strength;
    }
    
    let setStrength = function(value) {
      
      ps.strength = value;
    }
    
    let numLives = function() {
      
      return ps.numLives;
    }
    
    let addPoints = function(delta) {
      
      ps.score += delta;
    }
    
    let addStrength = function(delta) {
      
      ps.strength += delta;
    }
    
    let addExtraLife = function() {
    
      ps.numLives = ps.numLives + 1;
    }
    
    //}
    
    
    //{ Pubic interface
    
    entity.draw = draw;
    entity.anim_id = anim_id;
    entity.body = body;
    entity.position = position;
    entity.direction = direction;
    entity.score = score;
    entity.strength = strength;
    entity.setStrength = setStrength;
    entity.numLives = numLives;
    entity.addPoints = addPoints;
    entity.addStrength = addStrength;
    entity.addExtraLife = addExtraLife;
    
    entity.ContactProfile = Game.Model.ContactProfileInterface();

    entity.CollisionInterface = Game.Model.CollisionInterface(model.colliderType);
    
    // Platform binding interface (required by all platform-bindable objects)
    entity.PlatformBinding = {
    
      // Handle update if bound to a given platform.  T represents the translate vector of the platform for the current frame.
      update : function(T) {
        
        Matter.Body.translate(ps.body, T);
      }
    }
    
    //}
    
    
    entity.setCurrentState(model.initState, model.env, model.tDelta);
    
    return entity;
  }
  
  
  return module;
  
})((Game.Model || {}));