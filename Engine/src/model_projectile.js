
// Model projectile type

Game.Model = (function(module) {
  
  module.Projectile = module.Projectile || {};
  
  module.Projectile.Type = function(typeModel = {}) {
    
    let entity = {};
    
    //{ Type stores factory and sprite data
    
    let ps = {
      
      model : typeModel.model,
      sprite : typeModel.sprite,
      matterBody : typeModel.matterBody,
      states : typeModel.states,
      transitions : typeModel.transitions
    };
    
    //}
    
    
    //{ Public interface
    
    entity.model = function(owner, env, tDelta, overrideModel) {
      
      return ps.model(owner, env, tDelta, overrideModel);
    }
    
    entity.sprite = function() {
      
      return ps.sprite;
    }
    
    entity.matterBody = function(model) {
      
      return ps.matterBody(model, ps.sprite);
    }
    
    entity.states = function() {
      
      return ps.states;
    }
    
    entity.transitions = function() {
      
      return ps.transitions;
    }
    
    //}
    
    return entity;
  }
  
  
  
  module.Projectile.Instance = function(type, owner, env, tDelta, overrideModel, initialState) {
    
    let entity = Game.Model.Entity();
    
    //{ Private state
    
    let model = type.model(owner, env, tDelta, overrideModel);
    
    let ps = {
      
      type : type,
      model : model,
      body : type.matterBody(model),
      hitTarget : false
    }
    
    ps.body.hostObject = entity;
    ps.body.projectileFlag = true; // test tracker through matter.js
    ps.body.allowStaticInteractions = true;
    
    Matter.World.add(env.world, ps.body);
    
    //}
    
    
    //{ State graph setup
    
    let states = type.states();
    for (var key in states) {
      
      if (states.hasOwnProperty(key) && states[key] !== null) {
        
        entity.addState(key, states[key]({ model : ps.model }));
      }
    }
    
    // Add transitions / arcs between states
    let transitions = type.transitions();
    for (let t of transitions) {
      
      entity.addTransition(t.source, t.target, t.cond);
    }
    
    //}
    
    
    //{ Private API
    
    let draw = function(context) {
    
      if (ps.body) {
        
        context.save();
        
        let pos = ps.body.position;
        
        context.translate(pos.x, pos.y);
        context.translate(-ps.type.sprite().image().width * ps.model.scale / 2, -ps.type.sprite().image().height * ps.model.scale / 2);
        ps.type.sprite().draw(context, { x : 0, y : 0, scale : ps.model.scale });
        
        context.restore();
        
        if (Game.config.show_bounding_volume) {

          let vertices = ps.body.vertices;
          
          context.beginPath();
          
          context.moveTo(vertices[0].x, vertices[0].y);
          
          for (let j = 1; j < vertices.length; ++j) {
          
            context.lineTo(vertices[j].x, vertices[j].y);
          }
          
          context.lineTo(vertices[0].x, vertices[0].y);
              
          // Render geometry
          context.lineWidth = 1;
          context.strokeStyle = '#FFFFFF';
          context.stroke();          
        }
      }
    }
    
    // Accessor methods
    
    let get_type = function() {
      
      return ps.type;
    }
    
    let get_model = function() {
      
      return ps.model;
    }
    
    let body = function() {
      
      return ps.body;
    }
    
    let hitTarget = function() {
      
      return ps.hitTarget;
    }
    
    let setHitTarget = function() {
      
      ps.hitTarget = true;
    }
    
    //}
    
    
    //{ Public interface
    
    entity.draw = draw;
    entity.type = get_type;
    entity.model = get_model;
    entity.body = body;
    entity.hitTarget = hitTarget;
    entity.setHitTarget = setHitTarget;
    
    entity.CollisionInterface = Game.Model.CollisionInterface(ps.model.colliderType);
    
    //}
    
    
    // Initialise current state
    entity.setCurrentState(initialState, env, tDelta);
    
    return entity;
  }
  
  
  return module;
  
})(Game.Model || {});
