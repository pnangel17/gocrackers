'use strict';

// Static scene elements

Game.Model = (function(module) {
  
  module.StaticElement = (module.StaticElement || {});
  
  module.StaticElement.Type = function(model) {
    
    let entity = {};
    
    let ps = {
      
      colliderType : model.colliderType || 'static_tile',
      collisionFilter : model.collisionFilter || Game.Model.CollisionFilter.StaticScenery,
      sprite : Game.Graphics.Sprite({imageURL : model.spriteURI}),
      boundingVolumeScale : model.boundingVolumeScale || { width : 1.0, height : 1.0 }     
    };    
    
    //{ Public interface
    
    entity.colliderType = function() {
      
      return ps.colliderType;
    };
    
    entity.collisionFilter = function() {
      
      return ps.collisionFilter;
    };
    
    entity.sprite = function() { 
    
      return ps.sprite;
    };
    
    entity.boundingVolumeScale = function() {
      
      return ps.boundingVolumeScale;
    };
    
    //}
    
    
    return entity;
  }
  
  
  module.StaticElement.Instance = function(model, world) {
    
    let entity = {};
    
    let boundingVolumeScale = model.type.boundingVolumeScale();
    
    //{ Private state
        
    let ps = {
      
      body : Matter.Bodies.rectangle(model.pos.x, model.pos.y, model.size.width * boundingVolumeScale.width, model.size.height * boundingVolumeScale.height,
                              {
                                  isStatic : true,
                                  isSensor : false,                                  
                                  collisionFilter : model.type.collisionFilter()
                              }),
      type : model.type,
      size : model.size
    };
    
    ps.body.hostObject = entity;
    Matter.World.add(world, ps.body);
    
    //}

    
    //{ Private API
    
    let draw = function(context) {
    
      if (ps.body) {
        
        context.save();
        
        var pos = ps.body.position;
        
        context.translate(pos.x, pos.y);
        context.translate(-ps.size.width / 2, -ps.size.height / 2);
        ps.type.sprite().draw(context, { x : 0, y : 0, dWidth:ps.size.width, dHeight:ps.size.height });
        
        context.restore();
        
        if (Game.config.show_bounding_volume) {

          
          var vertices = ps.body.vertices;
          
          context.beginPath();
          
          context.moveTo(vertices[0].x, vertices[0].y);
          
          for (var j = 1; j < vertices.length; ++j) {
          
            context.lineTo(vertices[j].x, vertices[j].y);
          }
          
          context.lineTo(vertices[0].x, vertices[0].y);
              
          // Render geometry
          context.lineWidth = 1;
          context.strokeStyle = '#FFFFFF';
          context.stroke();          
        }
      }
    }
      
    
    // Accessor methods
    
    let body = function() {
      
      return ps.body;
    }
    
    let type = function() {
      
      return ps.type;
    }
    
    //}
    
    
    //{ Public interface
    
    entity.draw = draw;
    entity.body = body;
    entity.type = type;
    entity.CollisionInterface = Game.Model.CollisionInterface(ps.type.colliderType());
    
    //}
    
    
    return entity;
  }  
  
  return module;
  
})((Game.Model || {}));