/*jslint es6*/
'use strict';

// Main game stage

Game.Stages = (function(module) {

  module.MainGameStage = function(params={}) {

  // Setup instance based on Stage base class
    let self = Game.Stages.Stage(params);


    // Private state
    let ps = {

      exitMainLoop : false,
      level : 0,

      colliders : null,

      keyboard : null,
      //background : null,

      player : null,
      camera : null,

      //staticEntities : [], // static entities for each matter.js static body - use for static tile collisions
      staticElements : [],
      dynamicElements : [],
      projectiles : [],
      
      maxTimeDelta : 0,
      maxTimeDeltaCaptureTime : 0,
      
      endGameEvalFunc : null
    };


    // Private API

    let drawHUD = function(context, canvas) {

      // Draw HUD
      context.fillStyle = '#FF0000';
      //context.font = '30pt Amatic SC';
      context.font = '24pt Caveat Brush';

      let x1 = canvas.width / 8;
      let x3 = canvas.width - x1;

      let livesString = 'Lives : ' + ps.player.numLives();
      let scoreString = 'Crackers : ' + ps.player.score() + '/60';

      let textMetrics = context.measureText(livesString);
      context.fillText(livesString, x1 - textMetrics.width / 2, 80);

      textMetrics = context.measureText(scoreString);
      context.fillText(scoreString, x3 - textMetrics.width / 2, 80);


      let maxBarLength = 200;
      let barX = (canvas.width / 2) - (maxBarLength / 2);
      let barY = 60;

      let gradient1 = context.createLinearGradient(barX, barY, barX + maxBarLength, barY);
      gradient1.addColorStop(0, '#FF0000');
      gradient1.addColorStop(0.5, '#FFFF00');
      gradient1.addColorStop(1.0, '#00FF00');


      context.strokeStyle = gradient1;
      context.beginPath();
      context.lineWidth = 20;
      context.moveTo(barX, barY);
      context.lineTo(barX + Math.max(Math.min(ps.player.strength() / 100 * maxBarLength, maxBarLength), 0), barY);
      context.stroke();
    };

    let drawScene = function(context, canvas) {

      // Clear background
      context.fillStyle = 'black';
      context.clearRect(0, 0, canvas.width, canvas.height);

      context.save();
      ps.camera.applyTransform(context, canvas);


      // Draw background (main background always fixed to viewplane location in world coordinates)

      let vp = ps.camera.viewplane();
      let cPos = ps.camera.pos();

      let L = Game.Levels.Level[ps.level];

      let background = Game.Levels.Level[ps.level].background;

      if (background) {

        if (background.maintainAspect == true) {

          let w = background.image.width();
          let h = background.image.height();
          let aspect = h / w;
          let dWidth = vp.width;
          let dHeight = dWidth * aspect;

          background.image.draw(context, {
            x : cPos.x - (dWidth / 2),
            y : cPos.y - (dHeight / 2),
            dWidth : dWidth,
            dHeight : dHeight });
        }
        else {

          background.image.draw(context, {
            x : cPos.x - (vp.width / 2),
            y : cPos.y - (vp.height / 2),
            dWidth : vp.width,
            dHeight : vp.height });
        }
      }

      let R = Game.Levels.Level[ps.level].overlays;

      if (R) {
        
        for (let i = R.layers.length-1; i>=0; i--) {

          let renderLevel = R.layers[i];

          let scale = renderLevel.sigmaOverride || Math.pow(R.sigma, -(i+1));
          let mode = renderLevel.mode || RenderLevelImageMode.Normal;
          let offset = renderLevel.offset || {x : 0, y : 0};

          // Calculate coords of top-left of viewport in world coord space
          let dx = cPos.x - (vp.width / 2);
          let dy = cPos.y - (vp.height / 2);

          context.save();

          context.translate(dx + (-dx * scale), dy + (-dy * scale));
          context.translate(offset.x, offset.y);

          switch(mode) {

            case RenderLevelImageMode.Normal:

              renderLevel.image.draw(context, {
                x : 0,
                y : 0,
                dWidth : renderLevel.image.width(),
                dHeight : renderLevel.image.height()
              });

              break;

            case RenderLevelImageMode.Fill:

              renderLevel.image.draw(context, {
                x : 0,
                y : 0,
                dWidth : L.tileWidth * L.mapDimension.width,
                dHeight : L.tileHeight * L.mapDimension.height
              });

              break;

            case RenderLevelImageMode.Tile:
              break;
          }

          context.restore();
        }
      }
      
      // Draw static tiles
      let iStart = Math.floor((cPos.y - (vp.height >> 1)) / L.tileHeight);
      let iSpan = Math.ceil(vp.height / L.tileHeight);

      let jStart = Math.floor((cPos.x - (vp.width >> 1)) / L.tileWidth);
      let jSpan = Math.ceil(vp.width / L.tileWidth);

      for (let i=iStart; i <= iStart + iSpan && i < L.mapDimension.height; ++i) {

        for (let j=jStart; j <= jStart + jSpan && j < L.mapDimension.width; ++j) {

          if (i >= 0 && j >= 0) {

            if (L.mapTileArray[i][j]>=1 && L.mapTileArray[i][j]<32) {

              let spriteIndex = L.mapTileArray[i][j];

              context.save();

              context.translate(j * L.tileWidth + (L.tileWidth / 2), i * L.tileHeight + (L.tileHeight / 2));
              //context.rotate(body.angle); // depricated

              context.translate(-L.tileWidth / 2, -L.tileHeight / 2);

              Game.Sprites[spriteIndex].draw(context, { x : 0, y : 0, dWidth : L.tileWidth, dHeight : L.tileHeight });

              context.restore();
            }
          }
        }
      }

      // Draw bounding volumes
      if (Game.config.show_bounding_volume) {

        for (let k=0; k<L.mapTileBodyArray.length; ++k) {

          let body = L.mapTileBodyArray[k];

          let vertices = body.vertices;

          context.beginPath();

          context.moveTo(vertices[0].x, vertices[0].y);

          for (let i = 1; i < vertices.length; ++i) {

            context.lineTo(vertices[i].x, vertices[i].y);
          }

          context.lineTo(vertices[0].x, vertices[0].y);

          context.lineWidth = 1;
          context.strokeStyle = '#FFF';
          context.stroke();
        }
      }

 
      // Draw static scenery elements - these are not tiles, but extend the scene with sprites
      // that might be beyond the tile size
      for (let obj of ps.staticElements) {

        obj.draw(context);
      }
      
      // Draw player
      ps.player.draw(context, canvas);

      // Draw dynamic elements array
      for (let obj of ps.dynamicElements) {

        obj.draw(context);
      }

      // Draw projectiles
      for (let obj of ps.projectiles) {

        obj.draw(context);
      }
      
      context.restore();

      drawHUD(context, canvas);
    }

    let getPlayerPositionForLevel = function(playerIndex, level, world) {

      let position = null;

      for (let i=0; i<level.mapDimension.height && position === null; ++i) {

        for (let j=0; j<level.mapDimension.width && position === null; ++j) {

          if (level.mapTileArray[i][j]==-playerIndex) {

            position = { x : level.tileWidth * j + (level.tileWidth / 2), y : level.tileHeight * i + (level.tileHeight / 2)};
          }
        }
      }

      return position;
    }

    let getPickupsForLevel = function(level, world) {

      let numCrackers = 0;
      
      for (let i=0; i<level.mapDimension.height; ++i) {

        for (let j=0; j<level.mapDimension.width; ++j) {

          if (level.mapTileArray[i][j]<-1) {

            if (level.mapTileArray[i][j]<=-2 && level.mapTileArray[i][j]>=-4) {
              
              numCrackers++;
            }
            
            ps.dynamicElements.push(Game.Model.Pickup.Instance({

              type : Game.PickupTypes[level.mapTileArray[i][j]],
              pos : { x : level.tileWidth * j + (level.tileWidth / 2.0), y : level.tileHeight * i  + (level.tileHeight / 2.0)},
              boundingVolumeScale : 1.0,
              isStatic : true,
              initialState : 'Existing'
            },
            world));
          }
        }
      }
      
      console.log('num crackers to collect = ' + numCrackers);
    }
    
    let getStaticElementsForLevel = function(level, world) {
      
      for (let i=0; i<level.mapDimension.height; ++i) {

        for (let j=0; j<level.mapDimension.width; ++j) {

          if (level.mapTileArray[i][j]>=32) {

            let t = level.mapTileArray[i][j];
            let stype = Game.StaticElementTypes[t];
            
            // Find width and height in tiles: In the level array we just search the top row and left column ("inverted L") - but need to zero out rect so we don't instantiate other elements
            let widthInTiles = 1;
            let heightInTiles = 1;
            
            let k = j + 1;
            while (k < level.mapDimension.width && level.mapTileArray[i][k]===t) {

              widthInTiles++;
              k++;
            }
            
            k = i + 1;
            while (k < level.mapDimension.height && level.mapTileArray[k][j]===t) {

              heightInTiles++;
              k++;
            }

            // Zero-out elements in rect
            
            for (let ry=0; ry < heightInTiles; ++ry) {
            
              for (let rx=0; rx < widthInTiles; ++rx) {
                
                level.mapTileArray[i + ry][j + rx] = 0;
              }            
            }
            
            let w = widthInTiles * level.tileWidth;
            let h = heightInTiles * level.tileHeight;
            
            ps.staticElements.push(Game.Model.StaticElement.Instance({

              type : stype,
              pos : { x : level.tileWidth * j + (w / 2.0), y : level.tileHeight * i  + (h / 2.0)},
              size : {width : w, height : h}
            },
            world));
          }
        }
      }
    }

    let getDynamicElementsForLevel = function(level, world) {

      for (let obj of level.dynamicElements) {

        ps.dynamicElements.push(obj.factory(obj.model, world));
      }
    }

    let createStaticTileBodiesForLevel = function(level, world) {

      // Create body array to render bounding volumes from Matter.js
      level.mapTileBodyArray = [];
      let k = 0;

      let w = level.tileWidth;
      let h = level.tileHeight;

      for (let i=0; i<level.mapDimension.height; ++i) {

        for (let j=0; j<level.mapDimension.width; ++j) {

          if (level.mapTileArray[i][j]>=1 && level.mapTileArray[i][j]<32) {

            let x = w * j;
            let y = h * i;

            let r = j + 1; // start at next tile

            while(r < level.mapDimension.width && level.mapTileArray[i][r]>=1 && level.mapTileArray[i][r]<32) {

                  r++;
            };

            // n = number of tiles in RLE run
            let n = r - j;

            // Don't want to overlap bodies so start next iteration at end of this RLE run
            j = r;
            
            // New static entity for collisions
            let entity = Game.Model.StaticEntity({ colliderType : 'static_tile' });

            // New Matter.js body
            let body = Matter.Bodies.rectangle(x + (w * n) / 2, y + h / 2, w * n, h, { isStatic : true, collisionFilter : Game.Model.CollisionFilter.StaticScenery });
            
            body.hostObject = entity;
            
            level.mapTileBodyArray[k] = body;

            Matter.World.add(world, level.mapTileBodyArray[k]);
            k++;
          }
        }
      }
    }

    let setupColliderMap = function(objA, objB, handler, collisionEnd) {
      
      // Allocate new maps for collider types 'objA' and 'objB' if they do not already exist
      if (ps.colliders.has(objA) == false) {
        
        ps.colliders.set(objA, new Map());
      }
      
      if (ps.colliders.has(objB) == false) {
        
        ps.colliders.set(objB, new Map());
      }
      
      // Add colliders
      
      let keyA = (collisionEnd === undefined) ? objA : '-' + objA;
      let keyB = (collisionEnd === undefined) ? objB : '-' + objB;
      
      let collisionHandlerObj = {
        
        objA : objA,
        objB : objB,
        handler : handler
      };
      
      ps.colliders.get(objA).set(keyB, collisionHandlerObj);
      ps.colliders.get(objB).set(keyA, collisionHandlerObj);
    }
    
    let createColliders = function(level) {
      
      ps.colliders = new Map();
      
      // Iterate over colliders defined for current level behaviour
      for (let c of level.colliders) {
       
       let setA = c.objA.constructor===Array ? c.objA : [ c.objA ];
       let setB = c.objB.constructor===Array ? c.objB : [ c.objB ];
       
       for (let a of setA) {
         
         for (let b of setB) {
           
           setupColliderMap(a, b, c.handler, c.collisionEnd);
         }
       }
       
       /*
        // Allocate new maps for collider types 'objA' and 'objB' if they do not already exist
        if (ps.colliders.has(c.objA) == false) {
          
          ps.colliders.set(c.objA, new Map());
        }
        
        if (ps.colliders.has(c.objB) == false) {
          
          ps.colliders.set(c.objB, new Map());
        }
        
        // Add colliders
        
        let keyA = (c.collisionEnd === undefined) ? c.objA : '-' + c.objA;
        let keyB = (c.collisionEnd === undefined) ? c.objB : '-' + c.objB;
        
        ps.colliders.get(c.objA).set(keyB, c);
        ps.colliders.get(c.objB).set(keyA, c);*/
        
      }
      
      console.log(ps.colliders);
    }
    
    /*
    let foo = ['a', 'b'];
    let bar = 'a';
    console.log(foo.constructor===String);
    console.log(bar.constructor===Array);
    */
    
    // Sub-stages

    // Only init is exposed
    let init = function(params={}) {

      // Store current level index
      ps.level = (params.level || 0);

      // Setup keyboard handler
      ps.keyboard = (ps.keyboard || Game.KeyboardHandler());
      ps.keyboard.registerHandler();

      // Setup physics environment for current level
      let engine = Game.system.physicsEngine();
      let world = engine.world;

      // Setup additional properties
      //engine.positionIterations = 10;
      world.gravity.x = Game.config.world_gravity.x;
      world.gravity.y = Game.config.world_gravity.y;


      // Get current level model
      let L = Game.Levels.Level[ps.level];

      ps.endGameEvalFunc = L.endGameEvalFunc;
      
      // Create background model
      //let background = Game.Background(L.background);

      // Setup static tiles
      createStaticTileBodiesForLevel(L, world);

      // Setup player(s) and add to the dynamic elements array so it can be processed along with other elements
      if (L.players.model.pos === undefined) {

        L.players.model.pos = getPlayerPositionForLevel(1, L, world);
      }

      ps.player = L.players.factory(L.players.model, world);
      ps.dynamicElements.push(ps.player);
      
    
      // Setup pickups
      getPickupsForLevel(L, world);

      // Setup static (scenery) elements for level
      getStaticElementsForLevel(L, world);
      
      // Setup dynamic elements such as NPCs, movable platforms etc.
      getDynamicElementsForLevel(L, world);


      // Setup colliders
      createColliders(L);
      

      //{ Event handlers - All processing hangs off Matter.js update

      // beforeUpdate: Fired before engine update
      Matter.Events.on(engine, 'beforeUpdate', function(event) {

        // Handle state upates

        let env = {

          stage : self,
          world : event.source.world,
          system : Game.system
        };

        let timeDelta = Game.system.clock().deltaTimeInSeconds();

        // Update dynamic elements
        for (let i = ps.dynamicElements.length - 1; i >= 0; i--) {

          let obj = ps.dynamicElements[i];
          let alive = obj.update(env, timeDelta);

          if (!alive) {

            // Remove object from dynamic elements collection
            ps.dynamicElements.splice(i, 1);

            // Remove object from physics environment
            Matter.World.remove(event.source.world, obj.body());

          } else {

            // Before proceeding, clear contact profile of the object so Matter.js can rebuild it for the current frame via the collisionActive event callback below.  This isn't called for non-contacting objects, so we need to clear here in case no remaining contacts exist after the above update and Matter.js simulation step have occurred.

            if (obj.ContactProfile !== undefined) {

              obj.ContactProfile.resetContactProfile();
            }
          }
        }
        
        // Update projectiles
        for (let i = ps.projectiles.length - 1; i >= 0; i--) {

          let obj = ps.projectiles[i];
          let alive = obj.update(env, timeDelta);

          if (!alive) {

            // Remove object from dynamic elements collection
            ps.projectiles.splice(i, 1);

            // Remove object from physics environment
            Matter.World.remove(event.source.world, obj.body());
            
          } else {

            // Before proceeding, clear contact profile of the object so Matter.js can rebuild it for the current frame via the collisionActive event callback below.  This isn't called for non-contacting objects, so we need to clear here in case no remaining contacts exist after the above update and Matter.js simulation step have occurred.

            if (obj.ContactProfile !== undefined) {

              obj.ContactProfile.resetContactProfile();
            }
          }
        }
        
      });

      // collisionStart: Fired after engine update - process all new collisions that have started in the current iteration.
      Matter.Events.on(engine, 'collisionStart', function(event) {

        let pairs = event.pairs;

        for (let i=0; i<pairs.length; ++i) {

          let objA = pairs[i].bodyA.hostObject;
          let objB = pairs[i].bodyB.hostObject;

          if (objA !== undefined && objB !== undefined) {

            // Handle collision itself
            if (objA.CollisionInterface !== undefined &&
                objB.CollisionInterface !== undefined) {

              // Get Map of colliders for bodyA
              let collidersA = self.colliders().get(objA.CollisionInterface.type());

              if (collidersA !== undefined) {

                // From collidersA get actual collider function
                let colliderAB = collidersA.get(objB.CollisionInterface.type());

                if (colliderAB !== undefined) {

                  // Ensure objA / objB parameter order correct for collider handler
                  if (colliderAB.objA == objA.CollisionInterface.type()) {
                    
                    colliderAB.handler(objA, objB, { world : world, stage : self, pair : pairs[i] });
                    
                  } else {
                    
                    colliderAB.handler(objB, objA, { world : world, stage : self, pair : pairs[i] });
                  }
                }
              }
            }
            
          }

          // Manage contact profile

          let supports = pairs[i].collision.supports;
          let dist = Game.Model.Contact.calcContactDeviation(supports);

          if (objA!==undefined && objA.ContactProfile !== undefined) {

           objA.ContactProfile.updateContactProfile(objA.position(), supports, dist);
          }

          if (objB!==undefined && objB.ContactProfile !== undefined) {

            objB.ContactProfile.updateContactProfile(objB.position(), supports, dist);
          }

        }
      });

      // collisionActive: Fired after engine update - process all continuing / active collisions in the current iteration.
      Matter.Events.on(engine, 'collisionActive', function(event) {

        // Extract contact points for current frame and update contact profile
        let pairs = event.pairs;

        for (let i=0; i<pairs.length; ++i) {

          let objA = pairs[i].bodyA.hostObject;
          let objB = pairs[i].bodyB.hostObject;

          let supports = pairs[i].collision.supports;
          let dist = Game.Model.Contact.calcContactDeviation(supports);

          // Manage contact profile
          if (objA !== undefined && objA.ContactProfile !== undefined) {

            objA.ContactProfile.updateContactProfile(objA.position(), supports, dist);
          }

          if (objB !== undefined && objB.ContactProfile !== undefined) {

            objB.ContactProfile.updateContactProfile(objB.position(), supports, dist);
          }
        }
      });

      // collisionEnd: Fired after engine update - process all collisions that have ended in the current iteration.
      Matter.Events.on(engine, 'collisionEnd', function(event) {

        let pairs = event.pairs;

        for (let i=0; i<pairs.length; ++i) {

          let objA = pairs[i].bodyA.hostObject;
          let objB = pairs[i].bodyB.hostObject;

          if (objA !== undefined && objB !== undefined) {

            if (objA.CollisionInterface !== undefined &&
                objB.CollisionInterface !== undefined) {

              // Get Map of colliders for bodyA
              let collidersA = self.colliders().get(objA.CollisionInterface.type());

              if (collidersA !== undefined) {

                // From collidersA get 'end' collider function for bodyB
                let colliderAB = collidersA.get('-' + objB.CollisionInterface.type());

                if (colliderAB !== undefined) {

                  // Ensure objA / objB parameter order correct for collider handler
                  if (colliderAB.objA == objA.CollisionInterface.type()) {
                  
                    colliderAB.handler(objA, objB, { world : world, stage : self, pair : pairs[i] });
                  }
                  else {
                    
                    colliderAB.handler(objB, objA, { world : world, stage : self, pair : pairs[i] });
                  }
                }
              }
            }
          }
        }
      });

      // afterUpdate: Fired after engine update and all collision events processed
      Matter.Events.on(engine, 'afterUpdate', function(event) {

        // Handle state transitions

        let env = {

          stage : self,
          world : event.source.world,
          system : Game.system
        };

        for (let obj of ps.dynamicElements) {

          obj.processTransitions(env, Game.system.clock().deltaTimeInSeconds());
        }
        
        for (let obj of ps.projectiles) {

          obj.processTransitions(env, Game.system.clock().deltaTimeInSeconds());
        }
      });

      //}


      // Setup camera
      let aspect = Game.canvas.height / Game.canvas.width;

      let vpWidth = 400;
      let vpHeight = vpWidth * aspect;

      let pos = ps.player.position();

      ps.camera = Game.Camera({ pos : { x : pos.x, y : pos.y }, viewplane : {width : vpWidth, height : vpHeight } });


      // Update clock before any animation begins
      Game.system.clock().tick();


      window.requestAnimationFrame(mainLoop.bind(self));
    }

    let mainLoop = function() {

      // Void frame threshold:
      // Let normal frame occur in around 16-17ms gives approx 58 - 60fps
      // On initialisation we see heavy load time giving frame time around 63ms
      // For normal runtime we don't approach this.
      // However, let's run a 'normal frame time range' between 16 and 35ms.
      // Assume <16 not possible given browser refresh rate control ** investigate further **
      // So let 'void time' threshold be 35ms

      const deltaThreshold = 35; // ms

      // Update clock
      let timeDelta = Game.system.clock().tick(deltaThreshold);

      if (Math.abs(timeDelta) > ps.maxTimeDelta) {

        ps.maxTimeDelta = Math.abs(timeDelta);
        ps.maxDeltaCaptureTime = Game.system.clock().actualTimeElapsed();
      }

      // Update physics engine state
      if (timeDelta > 0) {

        Matter.Engine.update(Game.system.physicsEngine(), timeDelta);

        // Have camera follow player

        let L = Game.Levels.Level[ps.level];

        let vp = ps.camera.viewplane();

        let cameraPos = ps.camera.pos();
        let playerPos = ps.player.position();

        let vpWindow = {

          left : cameraPos.x - (vp.width / 6),
          right : cameraPos.x + (vp.width / 6),
          top : cameraPos.y - (vp.height / 8),
          bottom : cameraPos.y + (vp.height / 8)
        };


        // Apply movement window constraints
        if (playerPos.x < vpWindow.left) {

          cameraPos.x -= (vpWindow.left - playerPos.x);

        } else if (playerPos.x > vpWindow.right) {

          cameraPos.x += playerPos.x - vpWindow.right;
        }

        if (playerPos.y < vpWindow.top) {

          cameraPos.y -= (vpWindow.top - playerPos.y);

        } else if (playerPos.y > vpWindow.bottom) {

          cameraPos.y += playerPos.y - vpWindow.bottom;
        }


        // Apply camera bounds constraints

        let vpBounds = {

          left : 0,
          right : (L.tileWidth * L.mapDimension.width) - 1,
          top : 0,
          bottom : (L.tileHeight * L.mapDimension.height) - 1
        };

        cameraPos.x = Math.max(cameraPos.x - (vp.width / 2), vpBounds.left) + (vp.width / 2);
        cameraPos.x = Math.min(cameraPos.x + (vp.width / 2), vpBounds.right) - (vp.width / 2);

        cameraPos.y = Math.max(cameraPos.y - (vp.height / 2), vpBounds.top) + (vp.height / 2);
        cameraPos.y = Math.min(cameraPos.y + (vp.height / 2), vpBounds.bottom) - (vp.height / 2);


        ps.camera.pos(cameraPos);
      }


      // Draw new frame
      drawScene(Game.context, Game.canvas);

      // Show stats
      $('#actualTime').html('Seconds elapsed = ' + Game.system.clock().actualTimeElapsed());
      $('#timeDelta').html('Time Delta = ' + timeDelta /*Math.round(Game.system.clock().deltaTime())*/ );
      $('#fps').html('FPS = ' + Game.system.clock().averageFPS());
      $('#spf').html('SPF = ' + Game.system.clock().averageSPF());
      $('#maxTimeDelta').html('Max time delta = ' + ps.maxTimeDelta + ' at time index : ' + ps.maxDeltaCaptureTime);
      //$('#bindCount').html('Total bindings = ' + Game.Model.BindCount);

      // Process next iteration
      ps.exitMainLoop = ps.keyboard.isPressed('ESC') || ps.endGameEvalFunc(ps.player);

      if (ps.exitMainLoop) {

        window.requestAnimationFrame(leaveStage.bind(self));
      }
      else {

        window.requestAnimationFrame(mainLoop.bind(self));
      }
    }

    let leaveStage = function() {

      ps.keyboard.unregisterHandler();

      
      // Draw HUD
      Game.context.fillStyle = '#FF0000';
      Game.context.font = '80pt Caveat Brush';

      let x1 = Game.canvas.width / 2;

      var messageString;
      
      if (ps.player.score()==60) {
        
        messageString = 'CRACKING JOB!';
        
      } else {
      
        messageString = 'GAME OVER';
      }
      
      let textMetrics = Game.context.measureText(messageString);
      Game.context.fillText(messageString, x1 - textMetrics.width / 2, Game.canvas.height / 2);

      console.log('leaveStage');
    }


    // Accessor methods

    let keyboard = function() {

      return ps.keyboard;
    }

    let pickupArray = function() {

      return ps.pickupArray;
    }

    let colliders = function() {

      return ps.colliders;
    }

    let projectiles = function() {
      
      return ps.projectiles;
    }

    // Public interface
    self.init = init;
    self.keyboard = keyboard;
    self.pickupArray = pickupArray;
    self.colliders = colliders;
    self.projectiles = projectiles;
    
    return self;
  }


  return module;

})((Game.Stages || {}));