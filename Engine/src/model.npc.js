'use strict';

// Example NPC entity

Game.Model = (function(module) {
  
  module.FacingDirection = (module.FacingDirection || { Left : 0, Right : 1 });
  
  module.NPC = function(model, world) {
  
    let entity = Game.Model.Entity(model);
    
    //{ Private state
    
    let ps = {
    
      anim_id : model.anim_id,
      body : Matter.Bodies.rectangle(model.pos.x, model.pos.y, model.size.width, model.size.height, {
        //isStatic : true,
        //isSensor : true,
        collisionFilter : model.collisionFilter
      }),
      
      size : model.size,
      
      scale : model.scale || Game.config.default_npc_sprite_scale,
      
      boundingVolumeScale : model.boundingVolumeScale || 1.0,
      
      hitValue : model.hitValue || Game.config.default_npc_hit_value,
      
      strength : model.strength || Game.config.default_npc_strength,
      
      damage : model.damage || Game.config.default_npc_damage
    };
    
    ps.body.hostObject = entity;
    Matter.World.add(world, ps.body);
    
    //}
    
    
    //{ NPC states

    let AliveState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      state.enter = function(hostEntity, env, tDelta) {
      
        entity.setCurrentAnimationSequence(Game.Graphics.SequenceInstance({animationSequence : Game.Animation.sequence[ps.anim_id]['Alive']}));
      }
      
      state.update = function(hostEntity, env, tDelta) {
        
        entity.currentAnimationSequence().updateFrame(tDelta);        
        return true;
      }
            
      return state;
    }
    
    
    // EndState typically terminates a host character / entity.  This provides a convinient realisation of state terminator in a state diagram.
    let EndState = function(params = {}) {
      
      let state = Game.Model.State(params);
      
      state.update = function(hostEntity, env, tDelta) {
        
        return false;
      }
      
      return state;
    }
    
    //}
    
    
    //{ State-graph and transition setup
    
    entity.addState('Alive', AliveState());
    entity.addState('End', EndState());
    
    
    //}
    
    
    
    //{ Private API
    
    let draw = function(context, canvas) {
      
      // Draw main image rect
      context.save();
      
      context.translate(ps.body.position.x, ps.body.position.y);
      context.rotate(ps.body.angle);
      
      
      let anim = entity.currentAnimationSequence();
      
      if (anim !== null) {
        
        anim.drawCurrentFrame(ps.size, ps.scale, context);
        
      } else {
      
        context.fillStyle = 'rgb(0, 255, 0)';
        context.fillRect(-ps.size.width/2, -ps.size.height/2, ps.size.width, ps.size.height);
      }
      
      context.restore();
      
      
      if (Game.config.show_bounding_volume) {
      
        // Draw bounding volume (no model transform needed since already in world coords)
        
        let vertices = ps.body.vertices;
        
        context.beginPath();

        context.moveTo(vertices[0].x, vertices[0].y);
        
        for (let i = 1; i < vertices.length; ++i) {
        
          context.lineTo(vertices[i].x, vertices[i].y);
        }
        
        context.lineTo(vertices[0].x, vertices[0].y);
        
        context.lineWidth = 1;
        context.strokeStyle = '#FFF';
        context.stroke();
      }
      
    };
    
    
    // Accessor methods
    
    let body = function() {
    
      return ps.body;
    }
    
    let position = function() {
      
      return { x : ps.body.position.x, y : ps.body.position.y };
    }
    
    let damage = function() {
      
      return ps.damage;
    }
    
    let strength = function() {
      
      return strength;
    }
    
    let updateStrength = function(value) {
      
      ps.strength += value;
      
      console.log(ps.strength);
    }
    
    let hitValue = function() {
      
      return ps.hitValue;
    }
    
    //}
    
    
    
    //{ Pubic interface
    
    entity.draw = draw;    
    entity.body = body;
    entity.position = position;
    
    entity.damage = damage;
    entity.strength = strength;
    entity.updateStrength = updateStrength;
    entity.hitValue = hitValue;
    
    entity.CollisionInterface = Game.Model.CollisionInterface(model.colliderType);
    
    //}
    
    
    // Inital state entry
    entity.setCurrentState(model.initState, model.env, model.tDelta);
    
    return entity;
  }
  
  
  return module;
  
})((Game.Model || {}));