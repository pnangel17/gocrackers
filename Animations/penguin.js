Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});



	module.spritesheet['penguin'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/penguin.png',
		frameWidth : 32,
		frameHeight : 32,
		framesPerSecond : 12
	});


	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Standing_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 0,
		endFrame : 0,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Standing_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 0,
		endFrame : 0,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Walking_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 0,
		endFrame : 3,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Walking_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 0,
		endFrame : 3,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Jumping_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 4,
		endFrame : 7,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Jumping_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 4,
		endFrame : 7,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Falling_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 4,
		endFrame : 7,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence['player'] = (module.sequence['player'] || {});
	module.sequence['player']['Falling_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['penguin'],
		startFrame : 4,
		endFrame : 7,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));