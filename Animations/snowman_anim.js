Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});



	module.spritesheet['evil_snowman'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/evil_snowman.png',
		frameWidth : 128,
		frameHeight : 128,
		framesPerSecond : 1
	});


	module.sequence['evil_snowman'] = (module.sequence['evil_snowman'] || {});
	module.sequence['evil_snowman']['Alive'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['evil_snowman'],
		startFrame : 0,
		endFrame : 0,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));